﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerSpawner : MonoBehaviour {

    [SerializeField]
    GameObject towerPrefab;
    [SerializeField]
    Text towerCountText;
    int towerCount;

    void Awake() {
        towerCount = transform.childCount;
        towerCountText.text = "Towers: " + towerCount.ToString();
    }

    public void SpawnTower(Vector3 positon) {
        if(towerCount< 100) {
            GameObject tower = Instantiate(towerPrefab, transform);
            tower.transform.position = positon;
            towerCount = transform.childCount;
            towerCountText.text = "Towers: " + towerCount.ToString();
        }
        else {
            foreach(Transform child in transform) {
                TowerBehaviour tower = child.GetComponent<TowerBehaviour>();
                if(tower != null) {
                    tower.StartShootCoroutine();
                }
            }
        }
        
    }

   
}
