﻿
using UnityEngine;

public interface IBulletPoolObject  {

    void OnBulletSpawn(Transform parent);
}
