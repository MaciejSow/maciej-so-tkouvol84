﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour {

    public GameObject prefab;
    public int size = 150;
    public Queue<GameObject> objectQueue = new Queue<GameObject>();

    public static BulletPooler Instance;
    void Awake() {
        Instance = this;
        for (int i = 0; i < size; i++) {
            GameObject go = Instantiate(prefab, transform.parent);
            go.SetActive(false);
            objectQueue.Enqueue(go);
        }
    }


    public GameObject SpawnFromPool(Vector3 position, Quaternion rotation, Transform parent) {

        GameObject objToSpawn = objectQueue.Dequeue();
        objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;
        IBulletPoolObject pooledBullet = objToSpawn.GetComponent<IBulletPoolObject>();
        pooledBullet.OnBulletSpawn(parent);
        objectQueue.Enqueue(objToSpawn);
        return objToSpawn;
    }
}
