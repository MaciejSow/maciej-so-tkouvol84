﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : MonoBehaviour {

    [SerializeField]
    Bullet bulletPrefab;
    [SerializeField]
    BulletPooler bulletPooler;
    Renderer renderer;
    int numberOfShoots;

    void Start() {
        //bulletPooler = bulletPooler.GetComponent<BulletPooler>();
        renderer = gameObject.GetComponent<Renderer>();
        bulletPooler = BulletPooler.Instance;
        StartCoroutine(DelayBeforeTowerActivation());
    }

    public void StartShootCoroutine() {
        numberOfShoots = 0;
        StartCoroutine(ShootCoroutine());
    }

     IEnumerator ShootCoroutine() {
        while (true) {
            yield return new WaitForSeconds(0.5f);
            if (numberOfShoots < 12) {
                numberOfShoots++;
                int[] leftOrRight = { -1, 1 };
                transform.Rotate(Vector3.up * leftOrRight[Random.Range(0, 2)], Random.Range(15f, 45f));
                SpawnBullet();
            }
            else {
                renderer.material.color = Color.white;
            }
        }

    }

    IEnumerator DelayBeforeTowerActivation() {
        yield return new WaitForSeconds(2);
        renderer.material.color = Color.red;
        StartCoroutine(ShootCoroutine());
    }


    void SpawnBullet() {
        bulletPooler.SpawnFromPool(transform.position, transform.rotation, transform);
    }
}
