﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IBulletPoolObject {



    Vector3 startingPosition = new Vector3();
    Vector3 endingPosition;
    TowerSpawner towerSpawner;
    Transform originTower;

    void Start() {
        towerSpawner = FindObjectOfType<TowerSpawner>();
    }

    public void OnBulletSpawn(Transform parent) {
        originTower = parent;
        Debug.Log("bullet spawned");
        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * 4;
        startingPosition = gameObject.transform.position;
        endingPosition = Vector3.forward * Random.Range(1f, 4f);
    }

    void Update() {
        if (Mathf.Abs(transform.position.z) > Mathf.Abs(endingPosition.z)) {
            OnBulletDestroy();
            gameObject.SetActive(false);
        }
    }

    void OnBulletDestroy() {
        towerSpawner.SpawnTower(transform.position);
    }

    void OnCollisionEnter(Collision collision) {
        if(collision.transform != originTower) {
            Destroy(collision.gameObject);
            gameObject.SetActive(false);
        }
    }

}
